package BBor;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Scanner;

public class PointMain {
    static Scanner scanner = new Scanner(System.in);
    static ArrayList<Point> pointList = new ArrayList<Point>();

    public static void main(String[] args) {
        start();
        sortPoint();
        printList();
    }

    private static void start() {
        System.out.println("Ile punktów");
        int maxim = scanner.nextInt();
        for (int counterStart = 0; counterStart < maxim; counterStart++) {
            addPoint();
        }
    }

    private static void printList() {
        for (Point point :
                pointList) {
            System.out.println(point);
        }
    }

    public static void addPoint() {
        System.out.println("Podaj nazwe i x y ");
        String name = scanner.next();
        int x = scanner.nextInt();
        int y = scanner.nextInt();

        Point point = new Point(name, x, y);
        pointList.add(point);
    }

    private static void sortPoint() {
        Comparator<Point> pointComparator = (o1, o2) -> (int) (o1.getDistance() - o2.getDistance());
        pointList.sort(pointComparator);
    }
}


