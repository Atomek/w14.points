package BBor;

public class Point {
    private int xLabel;
    private int yLabel;
    private String name;

    public Point(String name, int xLabel, int yLabel) {
        this.name = name;
        this.xLabel = xLabel;
        this.yLabel = yLabel;
    }

    public double getDistance() {
        double distance = Math.sqrt(Math.pow(xLabel, 2) + Math.pow(yLabel, 2));
        return distance;
    }

    @Override
    public String toString() {
        return "Point{" +
                "xLabel=" + xLabel +
                ", yLabel=" + yLabel +
                ", name='" + name + '\'' +
                '}';
    }
}
